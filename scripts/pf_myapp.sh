#!/bin/bash 

count=$(kubectl get pods   -A --field-selector=status.phase!=Running |  grep -v Complete | wc -l)
while [ "$count" -ne 0 ]; do
echo "TestApp is starting..."
sleep 10
count=$(kubectl get pods -A --field-selector=status.phase!=Running | grep -v Complete | wc -l)
done

kubectl port-forward --address=192.168.56.150   service/my-service 3000:3000