#!/bin/bash 
count=$(kubectl get pods -n weave  -A --field-selector=status.phase!=Running | tee >( grep -v Complete) | wc -l)
while [ "$count" -ne 0 ]; do 
echo "Weave Scope is starting..."
sleep 10
count=$(kubectl get pods -n weave -A --field-selector=status.phase!=Running | grep -v Complete | wc -l)
done
kubectl port-forward --address=192.168.56.150   -n weave "$(kubectl get -n weave pod --selector=weave-scope-component=app -o jsonpath='{.items..metadata.name}')" 4040:4040